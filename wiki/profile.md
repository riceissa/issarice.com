---
title: Profile
author: Issa Rice
created: 2015-07-25
date: 2018-02-06
status: notes
---

On this page I'll present my results for various personality/morality tests, sort of like what [gwern has on his site](http://www.gwern.net/Links#profile), as well as what [Vipul Naik has on his site](https://vipulnaik.com/profile/).

# Personality

## Big Five

Taken on 2018-02-06; [test link](http://www.outofservice.com/bigfive/).

[![Big Five](big-five-2018-02-06.png)](big-five-2018-02-06.png)

## Autism spectrum quotient

Taken on 2018-02-05; [test link](https://psychology-tools.com/autism-spectrum-quotient/).

[![Autism spectrum
quotient](autism-spectrum-quotient-2018-02-05.png)](autism-spectrum-quotient-2018-02-05.png)

## Depression Anxiety Stress Scale

Taken on 2018-02-05; [test link](https://openpsychometrics.org/tests/DASS/).

[![Depression Anxiety Stress Scale](depression-anxiety-stress-scale-2018-02-05.png)](depression-anxiety-stress-scale-2018-02-05.png)

I think the results here are unusually low because I've managed to block off
most of my stressors.

## Nerdy Personality Attributes Scale

Taken on 2018-02-06; [test link](https://openpsychometrics.org/tests/NPAS/).

[![Nerdy Personality Attributes Scale](nerdy-personality-attributes-scale-2018-02-06.png)](nerdy-personality-attributes-scale-2018-02-06.png)

## Social Intelligence Test

Taken on 2018-02-05; [test link](http://socialintelligence.labinthewild.org/mite/).

[![Social Intelligence
Test](social-intelligence-2018-02-05.png)](social-intelligence-2018-02-05.png)

I should note that I took this test sometime around 2013--2014. I don't
remember what I got or if I still have the results.

## Systemising Quotient

Taken on 2018-02-06; [test link](https://www.aspietests.org/sq/index.php).

[![Systemising
Quotient](systemising-quotient-2018-02-06.png)](systemising-quotient-2018-02-06.png)

I think my score for this one is unusually low for two reasons: (1) I'm not
very systemizing about things like plants, animals, and airplanes (I'm more
systemizing about things like ideas and websites); (2) I tended to answer that
I have difficulty understanding e.g. maps and diagrams because I think they are
often poorly produced.

## IQ

I took the test at <http://www.iqtest.dk/> on 2018-02-06 and got a score of 112.

## Myers–Briggs Type Indicator

I seem to consistently test out as INPT, though I'm only heavily biased in the "I" part of the personality type and the "NPT" part is more intermediate.
(TODO: find actual test results.)

The MBTI is often mocked as “unscientific”, but I think
it can be quite useful; see “[On Types of Typologies]” for more.

[On Types of Typologies]: http://slatestarcodex.com/2014/05/27/on-types-of-typologies/

## Dungeons & Dragons alignment type

I got chaotic neutral when I took [the test](http://easydamus.com/alignmenttest.html) on or around 2014-09-07.
This was originally for [a Quora answer](https://www.quora.com/What-are-the-Dungeons-Dragons-alignment-types-of-prominent-Quora-users/answer/Issa-Rice).

> **Detailed Results:**
> 
> ```
> Alignment:
> Lawful Good ----- XXXXXXXXXXXXXXX (15)
> Neutral Good ---- XXXXXXXXXXXXXXXX (16)
> Chaotic Good ---- XXXXXXXXXXXXXXXXXXXX (20)
> Lawful Neutral -- XXXXXXXXXXXXXXXXXXX (19)
> True Neutral ---- XXXXXXXXXXXXXXXXXXXX (20)
> Chaotic Neutral - XXXXXXXXXXXXXXXXXXXXXXXX (24)
> Lawful Evil ----- XXXXXXX (7)
> Neutral Evil ---- XXXXXXXX (8)
> Chaotic Evil ---- XXXXXXXXXXXX (12)
> 
> Law & Chaos:
> Law ----- XXXXXX (6)
> Neutral - XXXXXXX (7)
> Chaos --- XXXXXXXXXXX (11)
> 
> Good & Evil:
> Good ---- XXXXXXXXX (9)
> Neutral - XXXXXXXXXXXXX (13)
> Evil ---- X (1)
> ```

# Morals

[to be added]

# Politics

According to [The Political Compass](https://www.politicalcompass.org/) (taken 2015-08-24) I am:

> Economic Left/Right: 1.25 
> Social Libertarian/Authoritarian: -6.0

Many of the question statements came across as odd to me, because of the way qualifiers were used.
For instance many statements used "some" to qualify statements, so that even if I believed the stereotypically libertarian position I would give the authoritarian-seeming answer.
