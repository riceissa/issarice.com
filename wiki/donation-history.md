---
title: Donation history
author: Issa Rice
date: 2018-01-03
---

My donation history begins in 2015 because that is the year during which I first started working/earning money that could be considered "my own".

# 2015

I earned around $2,000 doing mostly content creation work under Vipul Naik.
I did not donate in 2015 because I am still [building a financial buffer](https://www.quora.com/What-is-a-good-amount-of-financial-buffer-to-have-for-a-single-male/answer/Vipul-Naik).

# 2016

I earned around [$9,700][cw] (not counting deferred payments) working under
Vipul Naik.
I did not donate in 2016 because I am still building a financial buffer.

# 2017

I earned around $34,000. This amount includes the $9,360 deferred payment from
last year. My main source of income was contract work for Vipul Naik. I also
did some contract work for Peter Hurford (recorded on Vipul Naik's contract
work site) and sold leftover bitcoin that I had (worth around $250).

In addition, Vipul Naik offered me $500 to donate to a charity of my choice
(kind of a bonus for working for him all year).

I decided not to donate in 2017 (both with my own money and Vipul's offer,
which I deferred to a later time) due to a combination of (1) uncertainty about
my understanding of the world; and (2) continuing to build a financial buffer
(which is a smaller priority now that I've crossed the $30k threshold).

[cw]: https://contractwork.vipulnaik.com/worker.php?worker=Issa+Rice "“Contract work by Issa Rice for Vipul Naik”."
