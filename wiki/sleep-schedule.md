---
title: Sleep schedule
author: Issa Rice
date: 2017-10-16
---

This page is about my sleep schedule, i.e. the times of day during which I
sleep.

As of October 2017, I've been going to sleep between 10 PM and 1 AM and waking
up between 7 AM and 9 AM. I have been roughly keeping this schedule for several
months, although I don't have a record of when exactly it began.

Prior to this, I had tried many times to get up and go to sleep early, but
invariably tended to stay up late (to 2--4 AM), which forced me to wake up late
(10 AM--12 PM) as well.

I sleep anywhere from 5 to 11 hours per night (less sleep during school, since
I would be forced to wake up early even if I stayed up late).

At some point I used to regularly nap in the afternoon (and even had a period
where I wanted to experiment with polyphasic sleep), but as of October 2017 I
rarely nap, and all my sleep is in one continuous block at night.

I somewhat regularly take melatonin to make me sleepy at night. I bought a
bottle of 3 mg melatonin a couple of years ago, and each night I nibble off perhaps 1/8 to 1/4 of a pill.
