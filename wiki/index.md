---
title: Issa Rice
author: Issa Rice
toc: false
created: 2014-09-13
date: 2017-10-31
---

This is the website of myself, Issa Rice. This site stores biographical information
about me, but it is also a personal wiki or [open notebook](http://wcm1.web.rice.edu/open-notebook-history.html)
of sorts; anything that doesn't belong elsewhere ends up on here, and as a
result the content on here can be of admittedly low-quality: unfinished,
written in multiple voices over the span of years, wrong, or otherwise unsuitable for public
consumption. Embarrassing as it is, I like to "think and work in the open" so
others can benefit from my thoughts and critique them.

# On this site

- [About me](about), [about this site](about-this-site)
- [Contact me](contact): the best way to contact me is via email at
  [riceissa@gmail.com](mailto:riceissa@gmail.com)
- [Account names I use on various websites](account-names)
- [List of things I have worked on](work)
- [Software I use](software)
- My pages related to the [University of Washington](university-of-washington)
- [Notes on computing](computing) (Unix-like operating systems, programming, etc.)
- [Notes on math](math) (old)
- [Pages related to education](education)
- A mirror of [my Quora answers]()
- Information about my involvement with the [effective altruism](effective-altruism) movement
- Philanthropy:
    - [Open Philanthropy Project non-grant funding](open-philanthropy-project-non-grant-funding)
    - [GiveWell staff growth](givewell-staff-growth)
    - [Against Malaria Foundation](against-malaria-foundation)
    - [Total spending in global health](total-spending-in-global-health)
    - [Charities calculation](charities-calculation) (some data collection on the
      number of charities tracked under various criteria)

# Summary of what I worked on: November 2018 {#summary}

For completed tasks, see the [work]() page.

I spent some time adding to Vipul Naik's Learning Subwiki. You can see
[my contributions for the month](https://learning.subwiki.org/w/index.php?limit=150&title=Special%3AContributions&contribs=user&target=Issa+Rice&namespace=&tagfilter=&year=2018&month=11).

I learned about the GraphQL API (used on the new LessWrong 2.0 and EA Forum),
and wrote a simple reader for both [LessWrong 2.0](https://lw2.issarice.com/)
and [EA Forum](https://eaforum.issarice.com/). The source code is available on
[GitHub](https://github.com/riceissa/ea-forum-reader). I am also writing a
[tutorial](https://github.com/riceissa/ea-forum-reader/blob/master/tutorial.md)
because I found the learning process pretty painful and want to help other
people use the API.

I spent most of the month learning about things related to AI alignment. I
started a new [blog](https://issarice.wordpress.com/) to track updates on this
front.

For previous monthly updates, see the [archive of my monthly updates]().

# Activity feeds

Here are some activity feeds that track what I've been working on:

- My [daily updates blog](https://issarice.wordpress.com/), which tracks my
  learning related to AI safety
- My GitHub [contribution activity](https://github.com/riceissa), including my
  [commits to this website's
  repository](https://github.com/riceissa/issarice.com/commits/master)
- My [contributions to the Timelines Wiki](https://timelines.issarice.com/wiki/Special:Contributions/Issa)
  ([Git version with more incremental changes](https://github.com/riceissa/issarice.com/commits/master/external/timelines.issarice.com), [page creations](https://timelines.issarice.com/index.php?limit=50&title=Special%3AContributions&contribs=user&target=Issa&namespace=&tagfilter=&newOnly=1))
- My [contributions to the Machine Learning Subwiki](https://machinelearning.subwiki.org/wiki/Special:Contributions/IssaRice) ([page creations](https://machinelearning.subwiki.org/w/index.php?limit=50&title=Special%3AContributions&contribs=user&target=IssaRice&namespace=&tagfilter=&newOnly=1))
- My [contributions to the Learning Subwiki](https://learning.subwiki.org/wiki/Special:Contributions/Issa_Rice) ([page creations](https://learning.subwiki.org/w/index.php?limit=50&title=Special%3AContributions&contribs=user&target=Issa+Rice&namespace=&tagfilter=&newOnly=1))
- My [contributions to Devec, the Development Economics Subwiki](https://devec.subwiki.org/wiki/Special:Contributions/Issa_Rice) ([page creations](https://devec.subwiki.org/w/index.php?limit=50&title=Special%3AContributions&contribs=user&target=Issa+Rice&namespace=&tagfilter=&newOnly=1)).
- My [public GitHub Gists](https://gist.github.com/riceissa)
- [Summary of my contract work for Vipul
  Naik](https://github.com/vipulnaik/contractwork/blob/master/contributor-lists/issa-list.mediawiki)
  ([more detailed page with
  tables](https://contractwork.vipulnaik.com/worker.php?worker=Issa+Rice))
- [List of pages on this site, sorted by date of last substantive
  revision](all-pages)
- [Recent changes on the Cause Prioritization
  Wiki](https://causeprioritization.org/_activity)
- My recent [comments](https://www.greaterwrong.com/users/riceissa?show=comments)
  and [posts](https://www.greaterwrong.com/users/riceissa?show=posts) on LessWrong
- My recent [comments](https://ea.greaterwrong.com/users/riceissa?show=comments)
  and [posts](https://ea.greaterwrong.com/users/riceissa?show=posts) on the
  Effective Altruism Forum
- [Contributions to LessWrong
  Wiki](https://wiki.lesswrong.com/wiki/Special:Contributions/Riceissa)
- [Recent Quora questions](https://www.quora.com/profile/Issa-Rice/questions)
  (I occasionally ask questions on Quora while I work)

[email]: mailto:riceissa@gmail.com
