---
title: Hiring and judging character
author: Issa Rice
created: 2015-06-21
date: 2015-06-21
status: notes
---

- [We Can’t \(Simply\) Buy Capacity](http://blog.givewell.org/2013/08/29/we-cant-simply-buy-capacity/)
- the connection between hiring people for a job and judging the
  character of people fascinates me (e.g. for finding good friends, or
  for romantic relationships).
- both are clearly important: you need to hire people to get tasks done,
  and you want to find good friends/partner(s)
- both can take quite a bit of commitment to accurately judge
- both can end disastrously (due diligence)
- in both scenarios, there can be multiple people doing the judging, so
  there can be some gossip among those "in the know"
- sometimes it can be *really hard* to tell the difference between
  someone who is an *extremely* good fit, and someone who is a terrible
  fit.

- [How do you distinguish 10X software engineers from the rest?](https://www.quora.com/How-do-you-distinguish-10X-software-engineers-from-the-rest)

    -   Yishan Wong:

        > Look to referrals of referrals.  Look for the best person
        > according to the best person according to the best person you
        > know (yes, that's two levels).  If there is consensus among
        > several of your best people, this is a fairly reliable
        > indicator.  It is difficult to go more than 3x per "link"
        > though - people are typically unable to discern the difference
        > between someone 9x as good as they are from someone who is 3x
        > as good as they are; this is why you have to do the "best
        > person you know" referral twice.  First, you hire all the best
        > people you know, work with them for awhile to find the best
        > amongst them, and then find all the best people *they* know. 
        > It is a multiple-stage approach; many people don't know this,
        > and just assume that "refer the best people you know" is the
        > end of it.

        Similarly, stalking someone's friends might be useful if you
        want to determine their character.

        Similar is a [post by Eliezer Yudkowsky](https://www.facebook.com/yudkowsky/posts/10154648023319228):

        > People can't distinguish better or worse within more than one
        > standard deviation above their own level. I can't either: if there
        > were two superintelligences or just \>Eliezers arguing with each
        > other, to me they would both sound reasonable. (The "plus one
        > standard deviation" part isn't a standard result, just me trying to
        > eyeball the phenomenon.)

        Also similar is a [post by Caroline Frances Hubert](https://www.facebook.com/slickers.fantastic/posts/10210063725257144).
        In comments Pasha Kamyshev writes:

        > There are only a few reliable signals that someone can attempt to
        > use. A lot of verbal signals can be easy to fake. The strongest
        > signal that can't be faked is the ability to predict the future.
        > Another good one is the ability to pass the "Ideological Turing Test"
        > and explain both sides of an ideological argument. Another one is
        > your general *inability* to predict individual actions of the person,
        > while still roughly seeing the end-result outcome as positive.

        See also ["He Who Pays the Piper Must Know the
        Tune"](https://mason.gmu.edu/~rhanson/expert.pdf) by Robin Hanson.

        Also interesting is the IQ gap reported in leader--follower
        relationships.

        From [*Handbook of Organizational and Managerial
        Wisdom*][handbook_wisdom] (p 310):

        > The modest relationship of leadership and intelligence may occur
        > because it might not be intelligence per se that matters in
        > leadership; instead, it might be the relative intelligence of leaders
        > to their followers (Bass, 1990; Stogdill, 1948). Researchers have
        > suggested that leaders can be most successful when they are slightly
        > more intelligent, but not too much more intelligent, than their
        > followers. As Gibb (1969) put it, "The evidence suggests that every
        > increment of intelligence means wiser government, but that the crowd
        > prefers to be ill-governed by people it can understand" (p. 218).
        > This proposition was supported by Simonton (1985), who proposed
        > a curvilinear relationship between intelligence and a person's
        > influence over other group members (an indication of leadership),
        > such that there would be a high correlation between the group mean IQ
        > and the IQ of its most influential member, with a leader--follower IQ
        > gap of between 8 and 20 points, depending on organizational level,
        > and with smaller leader--follower gaps at more senior levels of an
        > organization's hierarchy. Thus intelligence may set the stage for
        > leadership, but many other factors are involved in successful
        > leadership as well.

- many more Quora questions about judging character (many by Alex):

    - [How to evaluate someone's ability to be an exceptional judge of character](https://www.quora.com/Judging-Character/How-do-you-evaluate-someones-ability-to-be-an-exceptional-judge-of-character)
    - [Is Elon Musk an exceptional judge of character/talent?](https://www.quora.com/Is-Elon-Musk-an-exceptional-judge-of-character-talent)
    - [How can you become a good judge of character?](https://www.quora.com/How-can-you-become-a-good-judge-of-character)
    - [How do you judge people's character?](https://www.quora.com/How-do-you-judge-peoples-character)
    - decent answer: [Sridatta Thatipamala's answer to How can you become a good judge of character?](https://www.quora.com/How-can-you-become-a-good-judge-of-character/answer/Sridatta-Thatipamala)
    - [How do I judge the character of a lady/girl?](https://www.quora.com/How-do-I-judge-the-character-of-a-lady-girl)

        - observing ppl under stress might be important

- one important question: does the person have the same life goals as me?

- "Are you at peace with your past romantic partners? Do you feel at peace with their joys though the joys may be mutually shared no longer?" ([source](https://www.quora.com/What-single-question-would-you-ask-someone-if-you-were-trying-to-find-out-if-they-are-a-good-person/answer/Chantal-Murthy-1))

- Yishan Wong([source](https://www.quora.com/What-is-the-best-way-to-indicate-in-a-job-posting-that-youre-looking-for-10x-or-ninja-type-people/answer/Yishan-Wong)):

    > the lesson here is that sometimes "10x people" are so good that
    > they don't really look for jobs in the same way other people do,
    > so you kind of have to find them via unorthodox means [...] [Y]ou
    > often have to make your job posting indicate a bit of quirkiness
    > in just the right way so that it happens to stick out to them when
    > they're casual perusing job ads 
    >
    > [...]
    >
    > the main idea is to visualize the personality and character of the
    > type of people you want to hire, and then put a message out
    > publicly that only those types of people will respond to or
    > provide a correct answer to.

- GiveWell has [The process of hiring our first cause-specific Program Officer](http://blog.givewell.org/2015/09/03/the-process-of-hiring-our-first-cause-specific-program-officer/).
Talking about senior hiring, they list some general principles:

    > -   The best way to evaluate someone is to work with them. The ideal way
    >     to make a senior hire would be to have a longstanding relationship
    >     with someone, perhaps as a part-time consulting arrangement.
    > -   Interviews are highly unreliable. The evaluation process should be
    >     designed as much as possible to mimic working together.
    > -   References are extremely important, and it can be useful to talk to
    >     many people about the candidate (including people who weren’t
    >     specifically offered as references).

    Also:

    > When a candidate didn’t mention a major aspect of the criminal
    > justice reform field, we would ask about it and see whether they
    > were omitting it because they (a) had strong knowledge of it and
    > were making a considered decision to de-prioritize this aspect of
    > the field; (b) didn’t have much experience or knowledge of this
    > area of the field.

    (So the general lesson here might be "do your homework" before trying to hire people.)

-   From "[Projects, People and Processes](http://www.openphilanthropy.org/blog/projects-people-and-processes)":

    > I generally consider it very hard to evaluate people, and don't know of
    > any reliable and reasonably quick way to do so. Our experiences
    > recruiting have generally left us feeling that the only good way to
    > evaluate someone is to work with them for an extended period of time, and
    > we've heard similar sentiments when seeking advice from other
    > organizations.

-   From "[The Most I'll Admit](http://econlog.econlib.org/archives/2017/01/the_most_ill_ad.html)":

    > My ability to discern human nobility is markedly worse than I thought in
    > 2015\. I've probably always been this bad, but 2016 helped me see my
    > limitations clearly.

- [Why Not To Break Up With Friends](http://www.jimterry.net/blog/why-not-to-break-up-with-friends/)

<https://www.facebook.com/satvik.beri/posts/610742700734>

[handbook_wisdom]: https://books.google.com/books?id=s5h2AwAAQBAJ&lpg=PA310&ots=aB_D0kMGsT&dq=IQ%20gap%20leader-follower%20relationship&pg=PA310#v=onepage&q=IQ%20gap%20leader-follower%20relationship&f=false "Eric H. Kessler and James R. Bailey. Handbook of Organizational and Managerial Wisdom. Sage Publications, Inc. 2007."
