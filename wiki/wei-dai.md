---
title: Wei Dai
created: 2018-01-17
date: 2018-01-17
status: draft
---

Something I find unfortunate is the apparent lack of enthusiasm for Wei Dai and his online content in the [effective altruism]() community. His [posts](http://lesswrong.com/user/Wei_Dai/submitted/) and [comments](http://lesswrong.com/user/Wei_Dai/comments/) on LessWrong seem to me to be clear, important, and relevant to effective altruism. And yet I rarely see his content referenced in the community (especially when compared to people like Nick Bostrom, Carl Shulman, and Eliezer Yudkowsky).

I wonder what the reasons are for this. Some ideas I can think of are:

* Wei is not really on board with altruism. You can see posts like ["Shut Up and Divide?"](http://lesswrong.com/lw/1r9/shut_up_and_divide/) and comments like [this](http://lesswrong.com/lw/cv9/building_toward_a_friendly_ai_team/6rvp) where he says "I think a highly rational person would have high moral uncertainty at this point and not necessarily be described as 'altruistic'." I don't think this is common knowledge though, so people who encounter just his philosophical ideas (without going through a lot of his output, and possibly being disappointed by his views on altruism) should still be enthusiastic.
* He often emphasizes uncertainty, so EAs might think there isn't much "actionable" insights in his writings. But EAs themselves [often seem uncertain](https://www.facebook.com/vipulnaik.r/posts/10213221225461126), so I don't think this really explains anything.
* Although Wei writes about philosophical problems, he doesn't write so much about empirical issues in global poverty and health.
* He doesn't seem to do a lot of self-promotion.
* He seems like a "quiet nerd" based on [this post](http://lesswrong.com/lw/66y/what_do_bad_clothes_signal_about_you/) about fashion. But isn't this true for many other people popular in EA?
* His anti-academia bent. He doesn't really write papers. (Even though he has an [impressive list](https://timelines.issarice.com/wiki/Timeline_of_Wei_Dai_publications) of online output.)
* He does not comment on Facebook very much.
* Maybe it's not so surprising that people aren't so enthusiastic about any particular person. There are probably other people who are like this that even I am not paying attention to.
* I'm wrong about one of my assumptions (that Wei's writings are relevant, or that EAs aren't paying attention).

Overall I'm still pretty confused about this.

"Yes, you're a freak and nobody but you and a few other freaks can ever get any useful thinking done" (Eliezer to Wei; [source](http://lesswrong.com/lw/5pf/what_were_losing/46xw "May 17, 2011. LessWrong."))

# External links

- [List of discussions between Paul Christiano and Wei Dai](https://causeprioritization.org/List_of_discussions_between_Paul_Christiano_and_Wei_Dai)
- [Wei Dai’s views on AI safety](https://causeprioritization.org/Wei_Dai%E2%80%99s_views_on_AI_safety)
