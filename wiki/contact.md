---
title: Contact
author: Issa Rice
date: 2018-11-01
---

The best way to contact [me](about) is via email at [riceissa@gmail.com][email].

If you prefer, you can friend me on [Facebook]() and message me there.

If you would like to ask me a question that I can answer publicly, you can use
my ["ask me anything" repository on GitHub](https://github.com/riceissa/ama) or
my [Tumblr "Ask me anything" form](https://riceissa.tumblr.com/ask). I will be
notified by email (via GitHub or Tumblr, respectively) but will respond publicly (if I choose to
respond).
Content is not shared between the GitHub repo and Tumblr, so e.g. a question
posted on GitHub will not show up on Tumblr.

You can also [send me anonymous feedback][feedback] (an idea I got from [Luke
Muehlhauser][l_feedb] and [Gwern][g_feedb]).

I try to respond to everyone who contacts me, but [for various reasons][noc] I
might not; feel free to [ping me][ping] if I don't respond.
Also see ["I emailed my customer asking multiple questions. Their reply
addressed only one. What is the polite way to point this out?"][multiq]; i.e. I
will try to respond to every question or point, but sometimes I might not (for
whatever reason).
If you really want an answer, feel free to re-ask the question/restate the
point.

See the page on [account names]() for account names I use on various websites.
You can try to contact me through those sites (if they offer a messaging
feature) but other than Gmail/Facebook I can't guarantee I will check them often.

You can stalk me online (e.g. by following me on Quora or Facebook or by
reading what I have on this website or my contributions to other sites).
I also don't mind if you ask questions about me on Quora, if you want to
passively contact me that way, though I don't control what Quora allows.

[email]: mailto:riceissa@gmail.com
[feedback]: https://docs.google.com/forms/d/1AbwmuMIyzB5X7P4ysL71vGD4WnMxsCKsAZULLc0X7V0/viewform?usp=send_form
[g_feedb]: http://www.gwern.net/About#anonymous-feedback
[l_feedb]: http://lesswrong.com/lw/8bt/tell_me_what_you_think_of_me/
[multiq]: https://workplace.stackexchange.com/questions/44483/i-emailed-my-customer-asking-multiple-questions-their-reply-addressed-only-one
[noc]: http://lesswrong.com/lw/eua/reasons_for_someone_to_ignore_you/ "“Reasons for someone to ‘ignore’ you” by Wei Dai"
[ping]: http://www.inc.com/minda-zetlin/why-you-need-to-be-better-at-following-up.html "The Art of Following Up (Without Being Annoying)"
