---
title: About
author: Issa Rice
created: 2014-09-13
date: 2016-11-13
---

*Note that I have two About pages. This page is about myself (Issa Rice); for information about this website, see [About this site](). See also [Colophon]().*

# Self-introduction

By day I am a freelance researcher. Since 2016 I have been regularly
working with [Vipul Naik](https://vipulnaik.com/), doing a mix of writing, programming, data collection, and other miscellaneous work. For the work I have
done for Vipul, see his [contract work portal page about me](https://contractwork.vipulnaik.com/worker.php?worker=Issa+Rice).

In my free time I do a mix of writing and programming as well. You can see
my [work]() page for a list of things I have worked on (the "Payment" column
being zero usually indicates I worked on something in my free time). This
website is also something that I have created in my free time, although most
pages are notes so I don't list them on my work page.

# Contact

The best way to contact me is via email at
[riceissa@gmail.com](mailto:riceissa@gmail.com).

See my [contact]() page for more ways to contact me.

See the page on [account names]() for account names I use on various websites.
You can try to contact me through those sites (if they offer a messaging
feature) but I can't guarantee I will check them often.

# Photo

I have a [photo](identification-photo.jpg) you can use to identify me.

# See also

- [Name]() for information about my name
