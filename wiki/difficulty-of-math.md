---
title: Difficulty of math
author: Issa Rice
created: 2018-09-06
date: 2018-09-06
# documentkind:
# status:
# belief:
---

I am interested in the difficulty of learning math, especially for
"smart" people.

<https://www.quora.com/Was-there-a-certain-age-when-abstract-physics-or-math-concepts-clicked-and-suddenly-made-sense-or-has-it-always-come-naturally-to-you/answer/Brian-Bi>

<https://www.quora.com/How-do-math-geniuses-understand-extremely-hard-math-concepts-so-quickly/answer/Satvik-Beri>

From ["Linear algebra: beware!"](https://files.vipulnaik.com/math-196/linearalgebrabeware.pdf):

> While linear algebra lacks any _single_ compelling visual tool, it
> requires _either_ considerable visuo-spatial skill _or_ considerable
> abstract symbolic and verbal skill (or a suitable linear combination
> thereof). Note the gap here: the standard computational procedures
> require only arithmetic. But getting an understanding requires
> formidable visuo-spatial and/or symbolic manipulation skill. So one
> can become a maestro at manipulating matrices without understanding
> anything about the meaning or purpose thereof.

<https://www.greaterwrong.com/posts/w5F4w8tNZc6LcBKRP/on-learning-difficult-things>

<https://www.facebook.com/vipulnaik.r/posts/10201718168211884>

Christopher Olah, in quotes like the [following](http://colah.github.io/posts/2015-08-Backprop/):

> Derivatives are cheaper than you think. That’s the main lesson to
> take away from this post. In fact, they’re unintuitively cheap, and
> us silly humans have had to repeatedly rediscover this fact. That’s
> an important thing to understand in deep learning. It’s also a
> really useful thing to know in other fields, and only more so if it
> isn’t common knowledge.

[Michael Nielsen on Simpson's paradox](http://www.michaelnielsen.org/ddi/if-correlation-doesnt-imply-causation-then-what-does/):

> Now, I’ll confess that before learning about Simpson’s paradox, I
> would have unhesitatingly done just as I suggested a naive person
> would. Indeed, even though I’ve now spent quite a bit of time
> pondering Simpson’s paradox, I’m not entirely sure I wouldn’t still
> sometimes make the same kind of mistake. I find it more than a
> little mind-bending that my heuristics about how to behave on the
> basis of statistical evidence are obviously not just a little wrong,
> but utterly, horribly wrong.

<https://www.facebook.com/qiaochu/posts/10155226053470811?comment_id=10155226054835811&reply_comment_id=10155226905650811&comment_tracking=%7B%22tn%22%3A%22R7%22%7D>

<https://en.wikipedia.org/wiki/Monty_Hall_problem#Confusion_and_criticism>

<https://mathoverflow.net/questions/38639/thinking-and-explaining>

<https://www.lesswrong.com/posts/2TPph4EGZ6trEbtku/explainers-shoot-high-aim-low>

Terry Tao and Tim Gowers also talk about similar things.

Not about math, but related is
<http://johnsalvatier.org/blog/2017/reality-has-a-surprising-amount-of-detail>.
In thought, there are things we didn't know that we don't know as well
as we think we do. I think this is related to ideas like cached
thoughts and the illusion of consciousness, where we end up taking
shortcuts or don't think about things carefully enough, so we have an
illusion of understanding something.

<https://jonahsinick.com/the-truth-about-mathematical-ability/>
